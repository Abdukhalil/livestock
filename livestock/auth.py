from django.contrib.auth.models import update_last_login
from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from core.helpers import send_message
from user.serializers import UserModelSerializer


class CustomTokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)
        user_data = UserModelSerializer(self.user, many=False).data
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user_type'] = user_data['user_type']

        if api_settings.UPDATE_LAST_LOGIN:
            update_last_login(None, self.user)

        return data

class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        send_message(request.data.get('username'))
        return super(CustomTokenObtainPairView, self).post(request, *args, **kwargs)

    def get_queryset(self):
        return


class CustomTokenRefreshView(TokenRefreshView):
    def post(self, request, *args, **kwargs):
        send_message(request.data.get('username'))
        return super(CustomTokenRefreshView, self).post(request, *args, **kwargs)
