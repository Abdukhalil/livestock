from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _


class Owner(models.Model):
    title = models.CharField(
        verbose_name='Название хозяйства',
        max_length=255
    )
    created = models.DateTimeField(
        null=True,
        auto_now_add=True
    )

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Хозяйство'
        verbose_name_plural = 'Хозяйства'


class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def create_user(self, username, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.is_active = True
        user.user_type = 'user'
        user.save()
        return user

    def _create_user(self, username, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        user = self.model(username=username, **extra_fields)
        user.is_active = True
        user.set_password(password)
        user.user_type = 'user'
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(username, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class UserTypeChoices(models.TextChoices):
        ADMIN = 'admin', 'Администратор'
        FARM_VETERINARIAN = 'farm_veterinarian', 'Врач'
        DISTRICT_INSPECTOR = 'district_inspector', 'Инспектор'
        OWNER = 'owner', 'Владелец'

    email = None
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Создан'))
    first_name = models.CharField(verbose_name=_('Имя'), max_length=255)
    last_name = models.CharField(verbose_name=_('Фамилия'), max_length=255)
    username = models.CharField(unique=True, verbose_name=_('Телефон'), max_length=255)
    verification_code = models.CharField(verbose_name='SMS Код подтверждения', max_length=8, null=True, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    user_type = models.CharField(_('Тип пользователя'),
                                 max_length=125,
                                 choices=UserTypeChoices.choices,
                                 default='farm_veterinarian',
                                 )
    date_joined = models.DateTimeField(auto_now_add=False,
                                       null=True,
                                       blank=True)
    owner = models.ForeignKey(
        Owner,
        verbose_name=_('Хозяйство'),
        null=True,
        on_delete=models.CASCADE
    )
    USERNAME_FIELD = 'username'
    objects = MyUserManager()

    def __str__(self):
        return self.username

    def fullname(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        verbose_name = 'Пользователи'
        verbose_name_plural = 'Пользователи'
