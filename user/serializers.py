from rest_framework import serializers

from user.models import User, Owner


class OwnerModelSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format="%d.%m.%Y %H:%M", required=False)

    class Meta:
        model = Owner
        fields = '__all__'


class UserModelSerializer(serializers.ModelSerializer):
    owner_data = OwnerModelSerializer(read_only=True, source='owner', many=False)

    class Meta:
        model = User
        fields = '__all__'
